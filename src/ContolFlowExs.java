public class ContolFlowExs {
    public static void main(String[] args) {


        // While
        //Create an integer variable i with a value of 9.
        //Create a while loop that runs so long as i is less than or equal to 23
        //Each loop iteration, output the current value of i, then increment i by one.

        int i = 9;
        while (i <= 23) {
            System.out.print(i + " ");
            i = i + 1;
        }

//   Do While
//Create a do-while loop that will count by 2's
// starting with 0 and ending at 100.
// Follow each number with a new line.


        int count = 0;
        while (count <= 100) {
            System.out.println(count);
            count = count + 2;
        }

        // Follow each number with a new line.
        //Alter your loop to count backwards by 5's from 100 to -10

        int num = 100;
        do{
            System.out.println(num);
            num -= 5;
        } while (num >= -10);

        //Create a do-while loop that starts at 2,
        // and displays the number squared on each line
        // while the number is less than 1,000,000.
        // Output should equal:

        long big = 2;
        while (big <= 1000000.) {
            System.out.println(big);
            big = (big) * (big);
        }


                //FizzBuzz
                /* 1-100
                then multiples 3 print Fizz instead of Number
                mult of 5 print Buzz
                */
//                public static String fizzBuzz() {
//                    if (number % 15 == 0) {
//                        System.out.println("fizzbuzz");
//                } else if (number % 5 == 0) {
//                        System.out.println ("buzz");
//                } else if (number % 3 == 0) {
//                        System.out.println("fizz");
//                } System.out.println(String.valueOf(number));
//                }




    }
}
