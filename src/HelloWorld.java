import org.w3c.dom.ls.LSOutput;

import java.sql.SQLOutput;
import java.util.Scanner;
import java.util.function.BinaryOperator;
import java.util.function.DoubleFunction;

public class HelloWorld {
    //PSVM--ENTER
    public static void main(String[] args) {
        // SOUT-- ENTER
        System.out.println("Hello, World!");
//        //print (ln) line
//        System.out.print("code inside or the curly braces");


//        //      DATE TYPES
//// PRIMATIVE DATA TYPES
//        byte age = 12;
//        short myShort = -32768;
//        int myInt = -127;
//        long myLong = 1234456789L;
//        float myFloat = 123.342F;
//        double myDouble = 123.342;
//        char myChar = 'B';
//        boolean myBoolean = true;
//        String myText = "Hello";
//
////        byte	1 byte	Stores whole numbers from -128 to 127
//        short	2 bytes	Stores whole numbers from -32,768 to 32,767
//        int	4 bytes	Stores whole numbers from -2,147,483,648 to 2,147,483,647
//        long	8 bytes	Stores whole numbers from -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807
//        float	4 bytes	Stores fractional numbers. Sufficient for storing 6 to 7 decimal digits
//        double	8 bytes	Stores fractional numbers. Sufficient for storing 15 decimal digits
//        boolean	1 bit	Stores true or false values
//        char	2 bytes	Stores a single character/letter or ASCII values

//        int x = 10;
//        int y = 25;
//
//        System.out.println(x + y);
//        System.out.println("x times y = " + (x * y));
//
//        byte myByte2 = 127;
//
////        You should complete all of the following inside of your main method from the previous exercise. After each step, compile and run your code.
////        Create an int variable named favoriteNum and assign your favorite number to it, then print it out to the console.
//
//        int favoriteNum = 123;
//        System.out.println(favoriteNum);
////        Create a String variable named myName and assign your first name as a string value to it, then print the variable out to the console.
//        Double myName = 3.14;
//        System.out.println(myName);
////                Change your code to assign a character value to myName. What do you notice?
////        "C AN MAKE NUMBERS INTO strings";
////        String myName = "Henry";
////        Change your code to assign the value 3.14159 to myName. What happens?
////        "Double"; WORKS
////        Declare an long variable named myNum, but do not assign anything to it.
////        Long myNum =;
////        Next try to print out myNum to the console. What happens?
////        System.out.println(myNum);
////        Change your code to assign the value 3.14 to myNum. What do you notice?
////        myNum = 3.14;
////        Change your code to assign the value 123L (Note the 'L' at the end) to myNum.
////        Long myNum = 123L;
////        Change your code to assign the value 123 to myNum.
////        byte myNum = 123;
////        Why does assigning the value 3.14 to a variable declared as a long not compile, but assigning an integer value does?
////
////                Change your code to declare myNum as a float.
////        float myNum = F;
//        float myNum = 3.14f;
//
////                Assign the value 3.14 to it.
////                What happens? What are two ways we could fix this?
////                Copy and paste the following code blocks one at a time and execute them
//
////        int a = 10;
////        System.out.println(a++);
//////        =10
////        System.out.println(a);
////        = 11
//////
////        int zy = 10;
////        System.out.println(++zy); // 11
////        System.out.println(zy); // 11
//
////        What is the difference between the above code blocks? Explain why the code outputs what it does.
////                Try to create a variable named class. What happens?
////        Object is the most generic type in Java. You can assign any value to a variable of type Object. What do you think will happen when the following code is run?
////
//        String theNumberEight = "eight";
//        Object o = theNumberEight;
//        int eight = (int) o;
//        System.out.println(eight);

//        Copy and paste the code above and then run it. Does the result match with your expectation?
//                How is the above example different from this code block?
//        int eight = (int) “eight”;
////        What are the two different types of errors we are observing?
////                Rewrite the following expressions using the relevant shorthand assignment operators:
////        int x = 5;
////        x = x + 6;
//        x +=
////        int x = 7;
////        int y = 8;
////        y = y * x;
//        y *= x
//
////        int x = 20;
////        int y = 2;
////        x = x / y;
//        x /= y
////        y = y - x;
//        y -= x
//
//
//                // SHORTHAND ASSIGNMENT BinaryOperator
//        ++
//                ++
//                --
//                =+
//                -=
//                *=
//                /+
//

//        What happens if you assign a value to a numerical variable that is larger (or smaller) than the type can hold? What happens if you increment a numeric variable past the type's capacity?
//        Hint: Integer.MAX_VALUE is a class constant (we'll learn more about these later) that holds the maximum value for the int type.
//
//        int z = Integer.MAX_VALUE;
//        System.out.println(z);
//
//
//      String name = "Bravo";
//      System.out.format("Hello there, %s. Nice to see you.", name);
//
//      String greet = "Hola";
//      System.out.format("%s, %s", greet, name);
//
//      System.out.println(greet + ", " + name + "!");

      // SCANNER CLASS -GET INPUT FROM THE CONSOLE
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter something: "); // prompt the user to enter
        String userInput = scanner.next(); // obtaining the value the user inout
        System.out.println(userInput); // sout'ing the userInput value


    }  //END OF MAIN METHOD (DON'T DELETE)
}   // END OF CLASS (DON'T DELETE)
