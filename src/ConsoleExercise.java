import java.text.DecimalFormat;
import java.util.Scanner;

public class ConsoleExercise {
    public static void main(String[] args) {
        double pi = 3.14159;
//        System.out.println("The value of Pi is approximately " + pi);
//        System.out.format("The value of Pi is approximately %2f\n", pi);

        Scanner scanner = new Scanner(System.in);
//        System.out.println("Enter a integer");
//        int userInput = scanner.nextInt();
//        System.out.println(userInput);


//        Don't change the value of the variable, instead, reference one of the links above and use System.out.format to accomplish this.
//        Explore the Scanner Class
//        Prompt a user to enter a integer and store that value in an int variable using the nextInt method.
//        What happens if you input something that is not an integer?
//        Prompt a user to enter 3 words and store each of them in a separate variable, then display them back, each on a newline.
//                What happens if you enter less than 3 words?
//                What happens if you enter more than 3 words?
//                Prompt a user to enter a sentence, then store that sentence in a String variable using the .next method, then display that sentence back to the user.
//        do you capture all of the words?
//        Rewrite the above example using the .nextLine method.
//        Calculate the perimeter and area of Code Bound's classrooms


//        String userInput1;
//        String userInput2;
//        String userInput3;
//        System.out.println("Enter 3 words");
//        userInput1 = scanner.next();
//        System.out.println(userInput1);
//        System.out.println(userInput2);
//        System.out.println(userInput3);
//
//        //or
//
//        System.out.println("Enter three words");
//        String word1 = scanner.next();
//        String word2 = scanner.next();
//        String word3 = scanner.next();
//        System.out.println(word1 + "\n" + word2 + "\n" + word3);

//        Prompt a user to enter a sentence, then store that sentence in a String variable using the .next method, then display that sentence back to the user.
//        do you capture all of the words?
//
//        System.out.println("Enter a sentence");
//        String userInput = scanner.nextLine();
//        System.out.println(userInput);

//        Rewrite the above example using the .nextLine method.
//        Calculate the perimeter and area of Code Bound's classrooms
//        System.out.println("Enter the length of Rectangle:");
//        String length = scanner.nextLine();
//        int l = scanner.nextInt();


//        System.out.println("Enter the length of Rectangle:");
//        double length = scanner.nextDouble();
//        System.out.println("Enter the width of Rectangle:");
//        double width = scanner.nextDouble();
//        //Area = length*width;
//        double area = length*width;
//        System.out.println("Area of Rectangle is:"+area);

//        double length = 4.5;
//        double width = 8.0;
//        double area = length*width;
//        System.out.println("Area of Rectangle is:"+area)

        System.out.println(" what is l n w of class");
        String length = scanner.nextLine();
        float l = Float.parseFloat(length);

        String width = scanner.nextLine();
        float w = Float.parseFloat(width);

        System.out.println("The perimeter of the classroom is " + (2 * w + 2 * l));
        System.out.println("The area of the classroom is " + (w * l));



    }

} //END DONT DELETE
