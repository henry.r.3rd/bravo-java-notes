package Arrays;

public class mutiDimesionalArray {

    public static void main(String[] args) {

        //create a 2nd Array
        int[][] a = { {1,2,3},
                {4,5,6,9},
                {7}, {10}
        };

        //calculate length of each row
        System.out.println("First element in the first row " + a[0][0]); // 1
        System.out.println("Third element in the second row " + a[1][2]); // 6
        System.out.println(a[2][0]); // 7
        System.out.println(a[3][0]); // 10


    }
}
