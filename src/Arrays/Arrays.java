package Arrays;

import java.lang.reflect.Array;

public class Arrays {
    //arrays
    // list of date that contains zero or more items called elements
    //array can only be of one date-type with string array

    //syntax for array
    //declaring an array of datatype double assinging variable of prices

    double[] prices;
    int[] integers;
    String[] names;
    float[] floatingNumbers;

    public static void main(String[] args) {

        String[] names = new String[6];
        names[0] = "Adrian";
        names[1] = "Sandra";
        names[2] = "MaryAnn";
        names[3] = "Henry";
        names[4] = "Jonathan";
        names[5] = "Eric";
        System.out.println(names[3]); //= Henry
        System.out.println(names[0]); // = Adrian


        String[] avengers = {"Cap", "Iron Man", "Hulk", "Thor", "HawkEye", "Black Widow"};
        System.out.println(avengers.length);

//iterating over arrays
        int[] numbers = new int[5];
        numbers[0] = 1;
        numbers[1] = 2;
        numbers[2] = 3;
        numbers[3] = 4;
        numbers[4] = 5;

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }


        String [] languages = {"html", "css", "javaSC", "angular", "java"};
        for(int i = 0; i < languages.length; i++) {
            System.out.println(languages[i]);
        }

            for(String language : languages){
                System.out.println(language);
            }
//Ex iterating through 2d array
            int [] [] matrix ={
                    {1,2,3},
                    {4,5,6},
                    {7,8,9}
            };
            for(int i = 0; i < matrix.length; i++){
                System.out.println(Array.toString(matrix[i]));

            }
        }

}



