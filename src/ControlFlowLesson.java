import org.w3c.dom.ls.LSOutput;

import java.awt.datatransfer.StringSelection;
import java.awt.desktop.SystemSleepEvent;
import java.util.Scanner;

public class ControlFlowLesson {
    public static void main(String[] args) {
        // COMPARISION OPERATIONS
        System.out.println(5 != 2); //True or ( 5 != 5) is False

        System.out.println(5 >= 5);  //True
        System.out.println(5 <= 5);  // True

        // !=, <, >, <=, >=, ==

        // LOGICAL OPERATIONS || , &&

        System.out.println(5 == 6 && 2 > 1 && 3 != 3); //False
        System.out.println(5 != 6 && 2 > 1 && 3 == 3); //True

        System.out.println(5 == 5 || 3 != 3); // true ( T || F = True)

        // IF STATEMENTS
        /*
        Syntax:
         do task 1
         } else
         do task 2
         }
         else {
         do task 3
         }
         */
        int score = 34;
        if (score >= 50) {
            System.out.println("New High Score!");
        } else {
            System.out.print("Try again...");
        }
//  STRING COMPARISON
        Scanner sc = new Scanner(System.in);
        System.out.println("Would you like to continue? [y/N]");
        String userInput = sc.next();

        //Dont do --Boolean confirm = userInput == "y";
        //Instead DO THIS
        boolean confirm = userInput.equals("y");
        if (confirm == true) {
            System.out.println("Welcome to the jungle");
        } else if (userInput.equals("n")) {
            System.out.println("Welcome to the jungle.... u entered a lowweerrcase n");
        } else {
            System.out.println("Bye bye bye");
        }

        // DONT do this
        if (userInput == "y") {
            System.out.println(" Welcome to the jungle");
        }

        /* Switch Statement
        SYNTAX:
        Switch (variable used for "switching") {
        case firstCase: do task A;
                break;
        case secondCase: do task B;
                    break;
         default: do task C;
                break;
         */

        Scanner input = new Scanner(System.in);
        System.out.println("Enter your grade: ");
        String userGrade = input.nextLine().toUpperCase();
        switch (userGrade) {
            case "A":
                System.out.println("Excellent");
                break;
            case "B":
                System.out.println("Okay a B Grade");
                break;
            case "C":
                System.out.println("C  Grade");
                break;
            case "D":
                System.out.println("oh No D Grade");
                break;
            default:
                System.out.println("Fail... sad");
        }

// While Loop
        //SYNTAX:
        /*
        while(conditional) {
        // loop
        }
         */

        int i = 0;
        while (i < 10) {
            System.out.println("i is " + i);
            i++;
        }

        // do while Loop
        /*
        do {
        // statements
        } while (condition is true)
         */

        do {
            System.out.println("You will see this!");
        } while (false);


        // FOR LOOP
       for  (var a = 0; 1 <= 100; i ++) {
        System.out.println(i);
    }

//
//    //Escape Squences - dealing with StringSelection
//    System.out.println("Hello \nWorld");
//    System.out.println("Hello \tWorld");
    }
}
