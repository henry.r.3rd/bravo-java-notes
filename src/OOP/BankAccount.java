package OOP;
/**Object - an instance of a class
 * -has properties and methods
 * -Instantiated with the new keyword
 * Class - template or blue-print that we use for objects
 */
//creating class
public class BankAccount {
    //creating instance variable
    public double balance;
    //creating static variable
    public static double interestRate;
    //Creating a static method
    public static double getInterestRate(){
        return interestRate;
    }
    public static double setInterestRate(double ir){
        interestRate = ir;
        return ir;
    }
    public double getBalance(){
        return balance;
    }
    public void setBalance(double balance){
        this.balance = balance;
    }
}
