package OOP;
public class BankAccountDemo {
    public static void main(String[] args) {
        //creating an account
        BankAccount a = new BankAccount();
        a.setBalance(88);
        //creating another account
        BankAccount b = new BankAccount();
        b.setBalance(99);
        System.out.println(a.getBalance());
        System.out.println(b.getBalance());
        BankAccount.setInterestRate(3.75);
        System.out.println("First: $" + a.getInterestRate());
        System.out.println("Second : $" + b.getInterestRate());
        System.out.println("Interest rate is " + BankAccount.getInterestRate() + "%");
    }
}
