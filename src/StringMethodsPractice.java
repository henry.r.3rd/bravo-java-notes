import org.w3c.dom.ls.LSOutput;

import java.lang.reflect.Array;
import java.util.Arrays;

public class StringMethodsPractice {
    public static void main(String[] args) {

        //Copy the following code into your main method.
           String myLength = "Good afternoon, good evening, and good night";
            //Length of out string??
           int nameLength = myLength.length();
        System.out.println("The length " + myLength + "contains" + nameLength + "Letters.");

        //Sephs code
        System.out.println(myLength.length());
        //String all uppercase
        System.out.println(myLength.toUpperCase());
        //String to lower case
        System.out.println(myLength.toLowerCase());

        String firstSubstring = "Hello World".substring(6);
        System.out.println(firstSubstring);

        String message = "Good evening, how are you?";

        //Print out only Good evening
        System.out.println(message.substring(0,12));

        //Pringt out only how are you
        System.out.println(message.substring(14, 26));

        //Create a char variable named 'myChar' and assign the value "San Antonio"
        //Using the charAt() method return the capital 'S' only.
          //      Change the argument in the charAt() method to where it returns the capital 'A' only.

        String myChar = "San Antonio";
        var results = myChar.charAt(0);
        System.out.println(results);
        // or
        char result = myChar.charAt(0);
        System.out.println(result);

        //Change the argument in the charAt() method to where it returns the FIRST lowercase 'o' only.
        System.out.println(myChar.charAt(7));

        //JAVE SYNTX
        //data-Type  variableNAME = Value;
        String bravo = "Henry MaryAnn Eric Sandra Jon Adrian";
        String[] splitBravo = bravo.split(", ");
        System.out.println(Arrays.toString(splitBravo));
        //or
        //String bravo = "Henry, MaryAnn, Eric, Sandra, Jon, Adrian";
        //String[] splitBravo = bravo.split(" ");
        //System.out.println(Arrays.toString(splitBravo));

        //Use the following string variables.
        String m1 = "Hello, ";
        String m2 = "how are you?";
        String m3 = "I love Java!";
        //Using concatenation, print out all three string variables that makes a complete sentence.;
        System.out.println(m1 + m2 + " " + m3);

        //Use the following integer variable
        //----->int result = 89;
        //Using concatenation, print out "You scored 89 marks for your test!"
        //*89 should not be typed in the sout, it should be the integer variable name.
        int result2 = 89;
        System.out.println("You scored " + result2 + "marks for your test!");

        //STRING DRILL
        //String Basics. Create a class named StringDrill with a main method.
        //    * For each of the following output examples, create a string variable named message that contains the desired output and print it out to the console.
        //    * Do this with only one string variable and one print statement.

        String lyrics = "I never wanna hear you say\nI want it that way! ";
        System.out.println(message);
        //Check "this" out!, "s inside of "s!
        String lyric2 = "Check \"this\"out!, \"s inside of \"s!\"";
        System.out.println(message);

        String message3 = "In windows, the main drive is usually C:\\\n" +
                "I can do backslashes \\, double backslashes \\\\,\n" +
                "and the amazing triple backslash \\\\\\!";

        //BONUS
        //Create date format converter application.
        //Take in the following format:
        //MM/DD/YYYY
        //Output the following:
        //MonthName DD, YYYY
        //Example:
        //user input - 12/01/1999
        //console output - December 1, 1999












    } //END 1
}  //END 2
