public class MethoidLessn {
    public static void main(String[] args) {
        //METHODS.....a sequence of statements that preform a specific task.

        // CALL MY MAIN METHOD (GREETING) ()
       //------- greeting("Hello", "bravo");

        //-----System.out.println(returnFive());

//        System.out.println(yelling("hello world"));

       //==== System.out.println(message("Newsletter ", "REPLIED"));
        //=====System.out.println(message());

//        String s = "This is a string";
//
//        String changeMe = "Hello Bravo";
//        String secondString = "Hello Codebound";
//        changeString((changeMe));
//
//        System.out.println(changeMe);

        counting(13);

    } ///END of Main Method

    //Basic Syntaxaz for a method:
    //public static returnType methodName(param1, para2,) {
    //     we want the code to do
    // }

    // % STRING place holder---
//    public static String greeting(String name) {
//        return String.format("Hello %s!", name);
//    }

   //----- public static void greeting(String greet, String name){
        //System.out.printf("%s,%s\n", greet, name);
   // }

   public static int returnFive() {
        return 5;
   }

// ---

// PUBLIC - this is the Visibility modifier
//    defines whether or not other classes can "see" this method
//
//  STATIC- the presence of this keyword defines that the method
//      belongs to the class, as opposed to instance of it.

   // METHOD OVERLOADING
    // - defining multiple methods with the same name,
    //b ut with diffenty parameter type, pararmeter order,
    //or number of parameters

    public static String message() {
        return "This is an example of methods";
    }

    public static String message(String memo) {
        return memo;
    }

    public static String message(int code) {
        return "Code: " + code + "message";
    }

    public static String message(String memo, String status) {
        return memo + "\nStatus: " + status;
    }

    //PASSING PARAMETER TO METHODS
//    public static void changeString(String s) {
//        s = "This is a string";
//        System.out.println(s);
//    }

    // RECURSION - That aims to solve a problme by dviding it into small chgunks

    //counting 5 to 1 using recursion
    public static void counting(int num) {
        if (num <= 0) {
            System.out.println("All Done");
            return;
        }
        System.out.println(num);
        counting(num -1);
    }

}// END OF CLASS

