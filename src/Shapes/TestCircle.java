package Shapes;

public class TestCircle {

    public static void main(String[] args) {

// Declare an instance of Circle class called c1.
// Construct the instance c1 by invoking the "default" constructor
// which sets its radius and color to their default value.

        Circles c1 = new Circles();
// Invoke public methods on instance c1, via dot operator.

  System.out.println("The first circle is " + c1.getColor() + "and has radius of " + c1.getRadius()
    + " and area of " + c1.getArea());


 // Getting radius input

// Declare an instance of class circle called c2.
        Circles c2 = new Circles(2.0, "blue");
// Invoke public methods on instance c2, via dot operator.
        System.out.println("The second circle is " + c1.getColor() + "and has radius of " + c1.getRadius()
                + " and area of " + c1.getArea());


    }   // END MAIN

}  // END CLASS
