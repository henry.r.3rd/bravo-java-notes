package Shapes;

public class Circles {
    //  Two private instance variables: radius (of the type double)
    //  and color (of the type String), with default value of 1.0
    //  and "red", respectively.
    private double radius;
    private String color;
    //  Two overloaded constructors - a default constructor with
    //  no argument, and a constructor which takes a double
    //  argument for radius.

    public Circles() {
        radius = 1.0;
        color = "red";
    }

    //  Two public methods: getRadius() and getArea(),
    //  which return the radius and area of this instance, respectively.

    public Circles(double radius, String color) {
        this.radius = radius;
        this.color = color;
    }

    public double getRadius() {
        return radius;
    }
//returns the aREa of this circle instance
    public double getArea() {
        return radius * radius * Math.PI;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

//    public void setRadius(double radius) {
//        this.radius = radius;
//    }
//
//    public static void main(String[] args)
//    {





    } //END MAIN
//} // END CLASS
